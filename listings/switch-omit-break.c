switch (some_value)
{
case 0:
	doSomething();
	break;

case -1:
	specialError();
	// fall through

default:
	generalError();
	break;
}
